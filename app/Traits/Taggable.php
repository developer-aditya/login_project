<?php 
namespace App\Traits;

use App\Tag;

trait Taggable
{
    //creating some events
    protected static function bootTaggable()
    {
        static::updating(function($model){
           $model->tags()->sync(static::findTagsInContent($model->content)); //taggable trait which has tags() relationship,
        });
        static::created(function($model){
            $model->tags()->sync(static::findTagsInContent($model->content));
        });

    }
    private static function findTagsInContent($content)
    {
         preg_match_all('/@([^@]+)@/m', $content, $tags);

         return Tag::whereIn('name', $tags[1] ?? [])->get();
         
    }
}