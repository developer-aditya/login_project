<?php

namespace App;

use App\Traits\Taggable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use Taggable;
    protected $fillable = ['user_id','content'];
    
    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
     /**
     * Get all of the tags for the comment.
     */
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable')->withTimestamps();
    }
}
