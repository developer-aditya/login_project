<?php

namespace App;

use App\Traits\Taggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class BlogPost extends Model
{
    use Taggable;
    protected $table = 'blogposts';
    protected $fillable = ['title','content','user_id'];

    /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
    
    public function user(){
        return $this->belongsTo('App\User');
    }
    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable')->withTimestamps();
    }

    /**
     * Get the post's image.
     */
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
    
    public function scopeMostCommented(Builder $query){
        //comments_count
        return $query->withCount('comments')->orderBy('comments_count','desc');
    }

    
    //this is the recommended way for declaring event handlers
    public static function boot() {
        parent::boot();
        self::deleting(function($blogpost) { // before delete() method call this
            $blogpost->image()->each(function($image) {
                $image->delete(); // <-- direct deletion
            });
            $blogpost->comments()->each(function($comments) {
                $comments->delete(); // <-- raise another deleting event on Post to delete comments
            });
            // do the rest of the cleanup...
        });
    }
}
