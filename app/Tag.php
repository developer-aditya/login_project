<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function blogPosts()
        {
            /**
            * Get all of the posts that are assigned this tag.
             */
            // return $this->belongsToMany('App\BlogPost')->withTimestamps();
            return $this->morphedByMany('App\BlogPost','taggable')->withTimestamps();
        }

        public function comments()
        {
            /**
            * Get all of the comments that are assigned this tag.
             */
            return $this->morphedByMany('App\Comment','taggable')->withTimestamps();
        }
    
}
