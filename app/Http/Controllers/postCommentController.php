<?php

namespace App\Http\Controllers;

use auth;
use App\BlogPost;
use Illuminate\Http\Request;
use App\Http\Requests\StoreComment;
use App\Mail\CommentPosted;
use Illuminate\Support\Facades\Mail;

class postCommentController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth')->only(['store']);
    }
     // model-route binding ,here BlogPost work as a type hinting
    public function store(BlogPost $post,StoreComment $request)
    {
       $comment = $post->comments()->create([
         'content' => $request->input('content'),
         'user_id' => $request->user()->id
       ]);
      //  $request->session()->flash('status','Comment was created!');
      //  return redirect()->back();
      Mail::to($post->user)->send(
        new CommentPosted($comment)
      );
      return redirect()->back()->withStatus('Comment was created!');
    }
}
