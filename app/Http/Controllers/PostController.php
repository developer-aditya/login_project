<?php

namespace App\Http\Controllers;
use App\User;
use App\Image;
use App\BlogPost;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\StorePost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['create','store','edit','update','destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //DB::connection()->enableQueryLog();
       // $posts = BlogPost::all();
        // $posts = BlogPost::with('comments')->get();
        // foreach($posts as $post){
        //     foreach($post->comments as $comment){
        //        echo $comment->content;
        //     }
        // }
        //dd(DB::getQueryLog());
        return view('posts.index',
        ['posts'=> BlogPost::withCount('comments')->with('tags')->orderBy('created_at', 'desc')->get(),
        'mostCommented'=>BlogPost::mostCommented()->take(5)->get(),
        'mostActive' =>User::withMostBlogPosts()->take(3)->get(),
        'mostActiveLastMonth' =>User::withMostBlogPostsLastMonth()->take(3)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('posts.create');
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $validatedata = $request->validated();
        $validatedata['user_id'] = $request->user()->id;
       // dd($request->all());
       $blogpost = BlogPost::create($validatedata);
       //file store
       $hasFile = $request->hasFile('thumbnail');

       if($hasFile)
       {
          $paths = $request->file('thumbnail');
          $path =  $paths->storeAs('imageThumbnail', $blogpost->id.'.'.$paths->guessExtension());

          $blogpost->image()->save(
             Image::make(['path'=>$path])
          );
        //   dump($path);
        //   dump($path->getClientMimeType());
        //   dump($path->getClientOriginalExtension());
        // //   $path->store('imageThumbnail');
        //   $url = $path->storeAs('imageThumbnail',$blogpost->id.'.'.$file->guessExtensioncreate());
        //   dump(Storage::url($url));
       }
       
    
    //    $blogpost = new BlogPost();
    //    $blogpost->title = $request->input('title');
    //    $blogpost->content = $request->input('content');
    //    $blogpost->save();
       $request->session()->flash('status','Blog post was created!');
       return redirect()->route('posts.show',['post'=>$blogpost->id]);
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function show($id)
    {
        return view('posts.show',
        ['post'=>BlogPost::with(['comments'=>function($query){
          return $query->latest();
        }])->with('tags')->with('user')->findOrFail($id),
        'mostCommented'=>BlogPost::mostCommented()->take(5)->get(),
        'mostActive' =>User::withMostBlogPosts()->take(3)->get(),
        'mostActiveLastMonth' =>User::withMostBlogPostsLastMonth()->take(3)->get()
        ]);
        //dd(BlogPost::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Blogpost::findorFail($id);
        return view('posts.edit',['post'=>$post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePost $request, $id)
    {
        $post = Blogpost::findorFail($id);
        //same work as gate 
        //$this->authorize('posts.update',$post);
        $this->authorize('update',$post);
        // if(Gate::denies('update-post',$post)){
        //     abort(403,"You can't edit this blog post");
        // }
        $validatedata = $request->validated();
        $post->fill($validatedata);

        $hasFile = $request->hasFile('thumbnail');

       if($hasFile)
       {
          $paths = $request->file('thumbnail');
          $path =  $paths->storeAs('imageThumbnail', $post->id.'.'.$paths->guessExtension());
          if($post->image){
              Storage::delete($post->image->path);
              $post->image->path = $path;
              $post->image->save();
          }else{
             $post->image()->save(
                Image::make(['path'=>$path])
             );
          }
        } 
        $post->save();
        $request->session()->flash('status','Blog post was updated!');
        return redirect()->route('posts.show',['post'=>$post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // BlogPost::destroy($id);
        $post = BlogPost::findorFail($id);
        // $result2 = Comment::where('blog_post_id', $id)->delete();
        // $result3 = Image::where('blog_post_id', $id)->delete();
        $this->authorize('delete',$post);
        // if(Gate::denies('delete-post',$post)){
        //     abort(403,"You can't delete this blog post");
        // }
        $post->delete();
        $request->session()->flash('status','Post was deleted successfully!');
        return redirect()->route('posts.index');
    }
}
