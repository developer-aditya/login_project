<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StorePost;
use Illuminate\Support\Facades\Gate;
use App\BlogPost;
use App\User;
use App\Tag;

class postTagController extends Controller
{
    public function index($tag)
    {
        $tag = Tag::findorFail($tag);
        return view('posts.index',
        ['posts'=>$tag->blogPosts()->withCount('comments')->with('user')->with('tags')->orderBy('created_at', 'desc')->get(),
        'mostCommented'=>BlogPost::mostCommented()->take(5)->get(),
        'mostActive' =>User::withMostBlogPosts()->take(3)->get(),
        'mostActiveLastMonth' =>User::withMostBlogPostsLastMonth()->take(3)->get()
        ]);
    }
}
