<?php

namespace App\Http\Controllers;

use auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreComment;

class UserCommentcontroller extends Controller
{
    public function __construct()
    {
      $this->middleware('auth')->only(['store']);
    }
     // model-route binding ,here BlogPost work as a type hinting
    public function store(User $user,StoreComment $request)
    {
        $user->commentsOn()->create([
         'content' => $request->input('content'),
         'user_id' => $request->user()->id
       ]);
       return redirect()->back()->withStatus('Comment was created!');
    }
}
