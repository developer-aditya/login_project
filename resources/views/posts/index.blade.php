@extends('layouts.front')
@section('content')
<div class="row">
    <div class="col-8">
@forelse ($posts as $post)
    <p>
    <h3><a href="{{route('posts.show',['post'=>$post->id])}}">{{$post->title}}</a></h3>
    {{-- <p class="text-muted">
        Added {{ $post->created_at->diffForHumans() }}
        by {{ $post->user->name }}
    </p> --}}
    @component('components.updated',['date'=>$post->created_at,'name'=>$post->user->name,'userId'=>$post->user->id])
        {{-- blank slot --}}
    @endcomponent
    @component('components.tags',['tags'=>$post->tags])
        {{-- blank slot --}}
    @endcomponent
    @if($post->comments_count)
        <p>{{$post->comments_count}} Comments</p>
        @else
        <p>No comments yet!!</p>
    @endif
    @can('update',$post)
    <a href="{{ route('posts.edit',['post'=>$post->id])}}" class="btn btn-primary">Edit Post</a>  
    @endcan
    {{-- @cannot('delete',$post)
        <p>You can not delete this post!</p>
    @endcannot --}}
    @can('delete',$post)
    <form action="{{route('posts.destroy',['post'=>$post->id])}}" method="POST" class="fm-inline">
        @csrf
        @method('DELETE')
      <input type="submit" value="Delete" class="btn btn-primary">
    </form> 
    @endcan
    </p>
    @empty
        <p>No Blog Post yet!</p>
@endforelse
</div>
<div class="col-4">
    @include('posts._activity')
</div>
</div>
@endsection
    