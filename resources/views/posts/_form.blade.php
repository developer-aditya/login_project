<div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="title" value="{{old('title',$post->title ?? null)}}">
</div>
<div class="form-group">
    <label>Content</label>
    <input type="text" class="form-control" name="content" value="{{ old('content',$post->content ?? null)}}">
</div>
<div class="form-group">
    <label for="thumbnail">Thumbnail</label>
    <input type="file" name="thumbnail" id="" class="form-control-file" 
    value="{{ old('thumbnail') }}">
</div>
@error
@enderror