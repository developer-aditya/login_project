<div class="container">
    <div class="row">
       <div class="card" style="width: 100%;">
       <div class="card-body">
       <h5 class="card-title">Most Commented</h5>
        <h6 class="card-subtitle mb-2 text-muted">What people are currently talking about.</h6>
        </div>
        <ul class="list-group list-group-flush"> 
        @foreach ( $mostCommented as $post)
        <a href="{{route('posts.show',['post'=>$post->id])}}">
        <li class="list-group-item">{{ $post->title }}</li>
        </a>
        @endforeach
        </ul>
      </div>
   </div>

   <div class="row mt-4">
    <div class="card" style="width: 100%;">
    <div class="card-body">
    <h5 class="card-title">Most Active Users</h5>
     <h6 class="card-subtitle mb-2 text-muted">Users with most posts written</h6>
     </div>
     <ul class="list-group list-group-flush"> 
     @foreach ( $mostActive as $user)
     <li class="list-group-item">{{ $user->name }}</li>
     @endforeach
     </ul>
   </div>
</div>

<div class="row mt-4">
    <div class="card" style="width: 100%;">
    <div class="card-body">
    <h5 class="card-title">Most Active Users in last month</h5>
     <h6 class="card-subtitle mb-2 text-muted">Users with most posts written in last month</h6>
     </div>
     <ul class="list-group list-group-flush"> 
     @foreach ( $mostActiveLastMonth as $user)
     <li class="list-group-item">{{ $user->name }}</li>
     @endforeach
     </ul>
   </div>
</div>
</div>