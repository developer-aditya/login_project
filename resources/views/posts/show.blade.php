@extends('layouts.front')
@section('content')
<div class="row">
    <div class="col-8">
        @if($post->image)
         <div style="background-image: url('{{ asset('storage'.'/'.$post->image->path) }}'); 
           min-height: 400px; color: white; text-align: center; background-attachment: fixed; width: auto;">
             <h1 style="padding-top: 100px; text-shadow: 1px 2px #000;">
        @else
           <h1>
        @endif
<h1>{{$post->title}}
@if ((new carbon\carbon())->diffInMinutes($post->created_at) < 20)
@component('components.badge')
New Post one !!
@endcomponent
@elseif ((new carbon\carbon())->diffInMinutes($post->created_at) > 20)
@component('components.badge',['type'=>'primary'])
old Post !!
@endcomponent
@else
    Something else
@endif
</h1>
@if($post->image)
           </h1>
        </div>
        @else
    </h1>
        @endif
<p>{{$post->content}}<br>
    {{-- <img src="http://127.0.0.1:8000/storage/{{ $post->image->path }}" alt="" srcset=""/> --}}
    {{-- <img src="{{ asset('storage'.'/'.$post->image->path) }}" alt="" srcset=""> --}}
    {{-- <img src=" {{Storage::url( $post->image->path) }}" alt="" srcset=""/> --}}
    {{-- <img src="{{ $post->image->url() }}" alt="" srcset=""> --}}
@component('components.updated',['date'=>$post->created_at,'name'=>$post->user->name])
    {{-- empty slot --}}
@endcomponent
@component('components.updated',['date'=>$post->updated_at,'name'=>$post->user->name])
updated
@endcomponent
</p>
@component('components.tags',['tags'=>$post->tags])
        {{-- blank slot --}}
@endcomponent
<h4>Comments</h4>
{{-- @include('comments._form') --}}
@commentForm(['route'=> route('posts.comments.store',['post'=>$post->id])])
@endcommentForm
@commentList(['comments'=>$post->comments])
@endcommentList
{{-- @forelse ($post->comments as $comment)
    <p>
        {{ $comment->content }} ,
        <p class="text-muted">
            @component('components.updated',['date'=>$comment->created_at,'name'=>$comment->user->name])
           {{-- blank slot --}}
           {{-- @endcomponent --}}
        {{-- </p>
    </p>
@empty
    <p>No Comments yet!!</p>
@endforelse --}} 
    </div>
    <div class="col-4">
        @include('posts._activity')
    </div>
@endsection