<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }
</style>

<p>Hi {{ $comment->commentable->user->name }}</p>
<p>
    Someone has commented on your blog post
    <a href="{{ route('posts.show',['post'=>$comment->commentable->id]) }}">
       {{ $comment->commentable->title }}
    </a>
</p>
<hr>
<p>
    <img src="{{ asset('http://127.0.0.1:8000/storage'.'/'.$comment->user->image->path) }}" alt=""/>
    <a href="{{route('users.show',['user'=>$comment->user->id]) }}">
    {{ $comment->user->name }}
</a> said:
</p>

<p>
"{{ $comment->content }}"
</p>