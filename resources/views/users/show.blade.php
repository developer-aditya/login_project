@extends('layouts.front')

@section('content')
    <div class="row">
    <div class="col-4">
        @if ($user->image)
        <img src="{{ asset('http://127.0.0.1:8000/storage/'.'/'.$user->image->path) }}"  class="img-thumbnail avatar"/> 
        @endif  
    </div>
    <div class="col-8">
        <h3>{{ $user->name }}</h3>
        @commentForm(['route'=> route('users.comments.store',['user'=>$user->id])])
        @endcommentForm
        @commentList(['comments'=>$user->commentsOn])
        @endcommentList
        <a href="{{ route('users.edit',['user'=>$user])}}" class="btn btn-primary">Edit Profile</a>
    </div>
   
    </div>
@endsection