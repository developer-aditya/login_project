@extends('layouts.front')

@section('content')
    <form method="POST" enctype="multipart/form-data" action="{{ route('users.update',['user'=> $user->id]) }}"
        class="form-horizontal">
        @csrf
        @method('put')
            <div class="row">
            <div class="col-4">
                @if ($user->image)
                <img src="{{ asset('http://127.0.0.1:8000/storage/'.'/'.$user->image->path) }}"  class="img-thumbnail avatar"/> 
                @endif  
                <div class="card mt-4">
                    <div class="card-body">
                        <h6>Upload a different photo</h6>
                        <input type="file" name="avatar" id="" class="form-control-file">
                    </div>
                </div>
                @error
               @enderror
            </div>
            <div class="col-8">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="" class="form-control" value="">
                </div>
                <div class="form-group">
                    <input type="submit" value="save changes!" class="btn btn-primary">
                </div>
            </div>
            </div>
    </form>
@endsection