    <div class="card" style="width: 100%;">
        <div class="card-body">
        <h5 class="card-title">{{ $title }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{ $subtitle }}</h6>
        </div>
        <ul class="list-group list-group-flush"> 
        @foreach ( $items as $item)
        {{-- <a href="{{route('posts.show',['post'=>$post->id])}}"> --}}
        <li class="list-group-item">{{ $item }}</li>
        </a>
        @endforeach
        </ul>
    </div>