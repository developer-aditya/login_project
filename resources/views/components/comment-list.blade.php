@forelse ($comments as $comment)
    <p>
        {{ $comment->content }} ,
        @component('components.tags',['tags'=>$comment->tags])
        {{-- blank slot --}}
        @endcomponent
        <p class="text-muted">
            @component('components.updated',
            ['date'=>$comment->created_at,'name'=>$comment->user->name,'userId'=>$comment->user->id])
        {{-- blank slot --}}
        @endcomponent
        </p>
    </p>
@empty
    <p>No Comments yet!!</p>
@endforelse